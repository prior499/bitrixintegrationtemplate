<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("������� ������");
?>

</style> <section class="description">
<h1 class="title-section" style="margin-top: -42px;">�������� �������� "DONNA-SAGGIA"</h1>
<div style="width:49%; float:left; min-width:150px;">
	<h2 style="color:#777879">��������</h2>
	<p>
		 �����: �. ������, ���������� �����, �. 125, ����. 3, ���. 1<br>
		 �������: 8 (985) 1111-016 � ��������� �����<br>
		 8 (916) 6111-119 � ������� ����� <br>
		 E-mail: <a href="mailto:mail@donna-saggia.ru">mail@donna-saggia.ru</a> <br>
		 Skype: donna-saggia
	</p>
	<h2 style="color:#777879">��� � ��� ���������</h2>
	<h3 style="color:#777879; text-transform:none; font-weight:bold">�a �����:</h3>
	<p>
		 ��. �. �����, ��������� ����� �� ������, �� ���������� ������ �������, ����� ����� ���������� ��������� ������ ������ �� ����������� �����. ������� ����� ��������� ������� �� ����������������� ������� ����� � ���������� �������� �� ����� �������. ����� ��������� ������˻ ������� ������ �� ����������� �������� � ��������� ����� ��������� ������, ��������������� ����������� �����. � ����� ������� 3-� ������ �� ���������� <strong>���������� �����, �. 125, ����. �, �� ������ (!) �� ����������</strong>&nbsp;<strong>��� ��. ��������</strong>, ������ �� ��� ����� � ���������� ������������� ������� (����������� �� ����). ��������� � �������� � ������� ������, ��� �� � �������� �Donna Saggia� (����� ������), ��� ������� � ����� ��������� ��� ������.
	</p>
	<h3 style="color:#777879; text-transform:none; font-weight:bold">�� ������:</h3>
	<p>
		 �������� �� ��. ������� �� ����������� �� ����������� ����� � ��. ��������, ���������� ������� �� ��������� ����� � ����� �������. ������ ����������� ����� � ������� � �������� �� �����������. �������� � ���, ���������� ��������� ���������. �������� ������� ������, � ��� ������� ������.<br>
	</p>
</div>
<div style="width:50%; float:left; min-width:150px; margin: 0 0 10px 10px;">
	<h2 style="color:#777879">����� ������� (<a href="https://yandex.ru/maps/213/moscow/?clid=1955454&amp;text=�.%20������%2C%20����������%20�����%2C%20�.%20125%2C%20����.%203%2C%20���.%201%2C������%2C%20������%2C%20����������%20�����%2C%20125�3�1&amp;sll=37.617855%2C55.621947&amp;sspn=0.032315%2C0.010394&amp;spn=0.001%2C0.001&amp;ll=37.619099%2C55.621984&amp;z=16&amp;ol=geo&amp;ouri=ymapsbm1%3A%2F%2Fgeo%3Fll%3D37.619%252C55.622%26spn%3D0.001%252C0.001%26text%3D%25D0%25A0%25D0%25BE%25D1%2581%25D1%2581%25D0%25B8%25D1%258F%252C%2520%25D0%259C%25D0%25BE%25D1%2581%25D0%25BA%25D0%25B2%25D0%25B0%252C%2520%25D0%2592%25D0%25B0%25D1%2580%25D1%2588%25D0%25B0%25D0%25B2%25D1%2581%25D0%25BA%25D0%25BE%25D0%25B5%2520%25D1%2588%25D0%25BE%25D1%2581%25D1%2581%25D0%25B5%252C%2520125%25D0%25BA3%25D1%25811&amp;rtext=~55.621984%2C37.619099&amp;rtt=auto" target="_blank">���������� �� ������.������</a>)</h2>
	<div style="margin-top:17px;">
 <img width="568" alt="map.jpg" src="/upload/medialibrary/deb/deb2ad7a5d4d2fde31ca9f66aa697748.jpg" height="425" title="map.jpg">
	</div>
</div>
 </section> <section class="news" style="clear:both; margin:0 -2.4%">
<div style="padding:0 2.4%; width:100%">
	<h1 class="title-section">�������� �������</h1>
	<h2 style="color:#777879">������ ������</h2>
	<div style="width:49%; float:left; min-width:150px;">
		<h3 style="color:#777879; text-transform:none; font-weight:bold">������� �����</h3>
		<ul style="font-size: 1.3rem; line-height: 2rem;">
			<li style="margin-left: 25px;">� 9:00 �� 18:00 � ������������ �� ������� (���-��� � 10:00 �� 18:00 �� ��������������� ������);</li>
			<li style="margin-left: 25px;">�������, ����������� � ����������� ��� � ��������;</li>
			<li style="margin-left: 25px;">������ ����� ���� ����������� ������������� � ��������� ��� ��������. </li>
		</ul>
	</div>
	<div style="width:49%; float:left; min-width:150px;">
		<h3 style="color:#777879;text-transform:none; font-weight:bold">��������� �����</h3>
		<ul style="font-size: 1.3rem; line-height: 2rem;">
			<li style="margin-left: 25px;">� 10:00 �� 19:00 � ������������ �� ������� (���-��� � 10:00 �� 20:00 �� ��������������� ������);</li>
			<li style="margin-left: 25px;">�������, ����������� � ����������� ��� � ��������;</li>
			<li style="margin-left: 25px;">������ ����� ���� ����������� ������������� � ��������� ��� ��������. </li>
		</ul>
	</div>
</div>
 </section> <section class="description">
<h1 class="title-section">���-���</h1>
<div style="width:49%; float:left; min-width:150px; margin-bottom:41px;">
	<h2 style="color:#777879">������ ������</h2>
	<ul>
		<li>� 10:00 �� 20:00 � ������������ �� ������� �� ��������������� ������ � ��������� �����;</li>
		<li>� 10:00 �� 18:00 � ������������ �� ������� �� ��������������� ������ � ������� �����;</li>
		<li>�������, ����������� � ����������� ��� � ��������;</li>
		<li>������ ����� ���� ����������� ������������� � ��������� ��� ��������;</li>
	</ul>
	<h3 style="color:#777879; text-transform:none; font-weight:bold">��� ����� ������</h3>
	<ul>
		<li>���������� �������� ��������;</li>
		<li>���������� WI-FI;</li>
		<li>�������� ���������;</li>
		<li>�������� �����������;</li>
		<li>������� ����������.</li>
	</ul>
	<p style="color: #777879; font-size: 16px;">
 <b>����������, �������� � ��������� �������� ��� ���-���, ����� �� ����� ������� ������������� ������� ��� ��� �����.</b>
	</p>
</div>
<div style="width:49%; float:left; min-width:150px;">
 <img width="100%" alt="���-��� Donna-Saggia" src="/upload/medialibrary/6ac/6ac7b4db795acbc106680d4b759021f1.jpg" title="���-��� Donna-Saggia" style="margin: 10px;">
</div>
 </section>
<div style="clear:both; margin-top:45px;">
</div>

		            </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>