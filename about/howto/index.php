<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("��� ������� �����?");
?>
<section class="description">
	<h1 class="title-section" style="margin-top: -42px;">��� ������� �����?</h1>
	<h3 style="color:#777879; text-transform:none; font-weight:bold">���������� ������</h3>
	<p>
		 ��� ����� ��� ����� ������� �� �������� "<a href="/tablitsa-razmerov/">������� ��������</a>" � �� ����������� ������� ������� ���������� ������.
	</p>
	<h3 style="color:#777879; text-transform:none; font-weight:bold">������� �����</h3>
	<h3 style="color:#777879; text-transform:none; font-weight:bold">�������� �����</h3>
	<p>
		 ����� ���� ��� ������������� ��� ����� ������ ����� �������� �����, ��� ����� ���� ������ �������:
	</p>
	<ol>
		<li style="margin-left: 25px;">
		<p>
 <strong>�������� ����� ��������-�������.</strong>&nbsp;��� ����� ��� �����:
		</p>
		<ul>
			<li style="margin-left: 45px;">
			<p>
				�������� �������������&nbsp;������ � �������.
			</p>
			</li>
			<li style="margin-left: 45px;">
			<p>
				������� �� ������, ����������� � ������ �������&nbsp;����� "<strong>�������</strong>". �� �������� "<strong>�������</strong>" ������������&nbsp;������ ��������� ������� � ��������� ������. �� ������ ��������&nbsp;���������� ���������&nbsp;�������&nbsp;��� �������&nbsp;�������� ������.
			</p>
			</li>
			<li style="margin-left: 45px;">
			<p>
				�����&nbsp;� �������&nbsp;������&nbsp;"<strong>�������� �����</strong>" ��������� ��������������� � �������� ���������� ������.
			</p>
			</li>
			<li style="margin-left: 45px;">
			<p>
				���� � ���� ������ �� �� ���� ������������ �� �����, �� ��� ����� ���������� ������&nbsp;<strong>��� ������������</strong>&nbsp;�&nbsp;<strong>������</strong>&nbsp;���&nbsp;������������������ (���� �� ���&nbsp;���������������� �� ����� �����), ��� ���� ���� �� �� ������� ���������������� �� ������ �������� ����� ��� ����������� �������� ��������������� �����.
			</p>
			</li>
			<li style="margin-left: 45px;">
			<p>
				���� ��&nbsp;������������, ��&nbsp;������ �������������� �� �������� "<strong>�������� �����".</strong>&nbsp;�� ���� �������� ��� ���������� ������ ��������� �����, ��� ���&nbsp;����� ����������&nbsp;�������&nbsp;<strong>�����</strong>&nbsp;�&nbsp;<strong>������� ��������</strong>, �&nbsp;<strong>����������� �����</strong>.
			</p>
			</li>
			<li style="margin-left: 45px;">
			<p>
				����� ������������� ������ �� ���&nbsp;e-mail ����� (��������� ��� �����������) ����� ������� ������ � ����������� � ������.
			</p>
			</li>
			<li style="margin-left: 45px;">
			<p>
				���� ��������� ����������� �������� � ���� ��� ��������� ������� ������!
			</p>
			</li>
		</ul>
 </li>
		<li style="margin-left: 25px;"><p><strong>��������� �� ��������</strong>, � ������� ����� ����� ���������.</p></li>
		<li style="margin-left: 25px;"><p><strong>�������� � ��� � ���-���.</strong>&nbsp;��� ����� ������ ������ ������, �.� � ��� � ���-���� �� ������� ��������, ��������, ����������, ������� ���������� ������ � ������ ������������� ����! � ���-���� ����������� ������ ����������� ���������, ������� ��� ����� ����� ������������ � ������ ����.
		</p>
		<p>
			 ��� ����� ��� ����� ������� ��� ��� ������� �&nbsp;<strong>������ 1</strong>, ������ ��� ������&nbsp;<strong>�������� ��������</strong>&nbsp;�������&nbsp;<strong>���������</strong>. ���� ���������&nbsp;�� �������� � �������� � ��������� ������� ������, � ��� �� ���� ������. ����� ������ ���-����, � ��� �� ����� �������� �� ������ ���������� �� ��������&nbsp;"<a href="http://donna-saggia.ru/contact">��������</a>".
		</p>
 </li>
		<li style="margin-left: 25px; margin-bottom: 0px !important;">
		<p style="margin-bottom: 0px !important;">
 <strong>��������� ����� � ��� �� ����������� �����</strong>:&nbsp;<a href="mailto:mail@donna-saggia.ru">mail@donna-saggia.ru</a>
		</p>
 </li>
	</ol>
 </section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>