<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $USER;

use Bitrix\Main,    
    Bitrix\Main\Localization\Loc as Loc,    
    Bitrix\Main\Loader,    
    Bitrix\Main\Config\Option,    
    Bitrix\Sale\Delivery,    
    Bitrix\Sale\PaySystem,    
    Bitrix\Sale,    
    Bitrix\Sale\Order,    
    Bitrix\Sale\DiscountCouponsManager,    
    Bitrix\Main\Context;
    
$request = \Bitrix\Main\Context::getCurrent()->getRequest();

$siteId = \Bitrix\Main\Context::getCurrent()->getSite();
$basket = Sale\Basket::loadItemsForFUser(\CSaleBasket::GetBasketUserID(), $siteId)->getOrderableItems();

$arResult['IS_EMPTY_BAG'] = empty($basket->getQuantityList());

$orderName = $request->getPost("CUSTOM_ORDER_NAME");
$orderPhone = $request->getPost("CUSTOM_ORDER_PHONE");
$orderEmail = $request->getPost("CUSTOM_ORDER_EMAIL");
$orderAddress = $request->getPost("CUSTOM_ORDER_ADDRESS");
$arResult['CHECK_NAME'] = true;
$arResult['CHECK_PHONE'] = true;
$arResult['CHECK_EMAIL'] = true;
$arResult['CHECK_ADDRESS'] = true;

if($request->getPost("CUSTOM_ORDER") == "Y" && !$arResult['IS_EMPTY_BAG']) {

	if(!check_email($orderEmail)) {
		$arResult['CHECK_EMAIL'] = false;
	}
	if(strlen($orderName) < 3) {
		$arResult['CHECK_NAME'] = false;
	}
	if(!preg_match("/^(\+)?([0-9]{2,6}\s?){2,5}$/", $orderPhone)) {
		$arResult['CHECK_PHONE'] = false;
	}
	if(strlen($orderAddress) < 5) {
		$arResult['CHECK_ADDRESS'] = false;
	}
	
	if(	$arResult['CHECK_NAME'] && $arResult['CHECK_EMAIL'] &&  
		$arResult['CHECK_PHONE'] && $arResult['CHECK_ADDRESS'] ) {

		if (!Loader::IncludeModule('sale'))
		    die();

		function getPropertyByCode($propertyCollection, $code)  {
		    foreach ($propertyCollection as $property)
		    {
		        if($property->getField('CODE') == $code)
		            return $property;
		    }
		}

		$currencyCode = Option::get('sale', 'default_currency', 'RUB');

		DiscountCouponsManager::init();

		$order = Order::create($siteId, \CSaleUser::GetAnonymousUserID());

		$order->setPersonTypeId(1);
		
		$order->setBasket($basket);

		/*Shipment*/
		$shipmentCollection = $order->getShipmentCollection();
		$shipment = $shipmentCollection->createItem();
		$shipmentItemCollection = $shipment->getShipmentItemCollection();
		$shipment->setField('CURRENCY', $order->getCurrency());
		foreach ($order->getBasket() as $item)
		{
		    $shipmentItem = $shipmentItemCollection->createItem($item);
		    $shipmentItem->setQuantity($item->getQuantity());
		}
		$arDeliveryServiceAll = Delivery\Services\Manager::getRestrictedObjectsList($shipment);
		$shipmentCollection = $shipment->getCollection();

		if (!empty($arDeliveryServiceAll)) {
		    reset($arDeliveryServiceAll);
		    $deliveryObj = current($arDeliveryServiceAll);

		    if ($deliveryObj->isProfile()) {
		        $name = $deliveryObj->getNameWithParent();
		    } else {
		        $name = $deliveryObj->getName();
		    }

		    $shipment->setFields(array(
		        'DELIVERY_ID' => $deliveryObj->getId(),
		        'DELIVERY_NAME' => $name,
		        'CURRENCY' => $order->getCurrency()
		    ));

		    $shipmentCollection->calculateDelivery();
		}
		/**/

		/*Payment*/
		$arPaySystemServiceAll = [];
		$paySystemId = 1;
		$paymentCollection = $order->getPaymentCollection();

		$remainingSum = $order->getPrice() - $paymentCollection->getSum();
		if ($remainingSum > 0 || $order->getPrice() == 0)
		{
		    $extPayment = $paymentCollection->createItem();
		    $extPayment->setField('SUM', $remainingSum);
		    $arPaySystemServices = PaySystem\Manager::getListWithRestrictions($extPayment);

		    $arPaySystemServiceAll += $arPaySystemServices;

		    if (array_key_exists($paySystemId, $arPaySystemServiceAll))
		    {
		        $arPaySystem = $arPaySystemServiceAll[$paySystemId];
		    }
		    else
		    {
		        reset($arPaySystemServiceAll);

		        $arPaySystem = current($arPaySystemServiceAll);
		    }

		    if (!empty($arPaySystem))
		    {
		        $extPayment->setFields(array(
		            'PAY_SYSTEM_ID' => $arPaySystem["ID"],
		            'PAY_SYSTEM_NAME' => $arPaySystem["NAME"]
		        ));
		    }
		    else
		        $extPayment->delete();
		}
		/**/

		$order->doFinalAction(true);
		$propertyCollection = $order->getPropertyCollection();

		$nameProperty = getPropertyByCode($propertyCollection, 'FIO');
		$nameProperty->setValue($orderName);
		$phoneProperty = getPropertyByCode($propertyCollection, 'PHONE');
		$phoneProperty->setValue($orderPhone);
		$emailProperty = getPropertyByCode($propertyCollection, 'EMAIL');
		$emailProperty->setValue($orderEmail);
		$addressProperty = getPropertyByCode($propertyCollection, 'ADDRESS');
		$addressProperty->setValue($orderAddress);

		$order->setField('CURRENCY', $currencyCode);
		// $order->setField('USER_DESCRIPTION', '����������� ����������');
		$order->save();
		$orderId = $order->GetId();
		$arResult['ORDER_PROCESSED'] = true;
	}
}

$this->IncludeComponentTemplate();
?>