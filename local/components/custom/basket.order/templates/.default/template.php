<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>


<?if($_POST['AJAX_CALL']) {
  $GLOBALS['APPLICATION']->RestartBuffer();
}?>

<?if(!$arResult['IS_EMPTY_BAG']):?>
  <?if($arResult['ORDER_PROCESSED']):?>
    <h1 class="title-section"><?=GetMessage("ORDER_SUCCESS")?></h1>  
  <?else:?>
    <div class="contact-form">    
      
      <?=CAjax::GetForm('id="form-custom-basket-order" method="POST" enctype="multipart/form-data"', 'form-custom-basket-order', '1')?>
      <form id="form-custom-basket-order" method="POST">
        <div class="title-section"><?=GetMessage("CONTACT_INFO_TITLE")?></div>
        <div class="form-line">
          <label><?=GetMessage("CONTACT_NAME_LABEL")?></label>
          <fieldset><div class="row">
            <?if(!$arResult['CHECK_NAME']):?>
              <p style="color: red;"><?=GetMessage("CHECK_CORRECT_NAME")?></p>
            <?endif;?>
            <input type="text" name="CUSTOM_ORDER_NAME" value="<?if(!empty($_POST['CUSTOM_ORDER_NAME'])) echo $_POST['CUSTOM_ORDER_NAME'];?>"></div></fieldset>
        </div>
        <div class="form-line">
          <label><?=GetMessage("CONTACT_PHONE_LABEL")?></label>
          <fieldset><div class="row">
            <?if(!$arResult['CHECK_PHONE']):?>
              <p style="color: red;"><?=GetMessage("CHECK_CORRECT_PHONE")?></p>
            <?endif;?>
            <input type="tel" name="CUSTOM_ORDER_PHONE" value="<?if(!empty($_POST['CUSTOM_ORDER_NAME'])) echo $_POST['CUSTOM_ORDER_PHONE'];?>"><p><?=GetMessage("CONTACT_PHONE_HINT")?></p></div></fieldset>
        </div>
        
        <div class="form-line">
          <label><?=GetMessage("CONTACT_EMAIL_LABEL")?></label>
          <fieldset><div class="row">
            <?if(!$arResult['CHECK_EMAIL']):?>
              <p style="color: red;"><?=GetMessage("CHECK_CORRECT_EMAIL")?></p>
            <?endif;?>
            <input type="email" name="CUSTOM_ORDER_EMAIL" value="<?if(!empty($_POST['CUSTOM_ORDER_NAME'])) echo $_POST['CUSTOM_ORDER_EMAIL'];?>"><p><?=GetMessage("CONTACT_EMAIL_HINT")?></p></div></fieldset>
        </div>
        
        <div class="form-line">
          <label><?=GetMessage("CONTACT_ADDRESS_LABEL")?></label>
          <fieldset><div class="row">
            <?if(!$arResult['CHECK_ADDRESS']):?>
              <p style="color: red;"><?=GetMessage("CHECK_CORRECT_ADDRESS")?></p>
            <?endif;?>
            <textarea name="CUSTOM_ORDER_ADDRESS"><?if(!empty($_POST['CUSTOM_ORDER_ADDRESS'])) echo $_POST['CUSTOM_ORDER_ADDRESS'];?></textarea><p><?=GetMessage("CONTACT_ADDRESS_HINT")?></p></div></fieldset>
        </div>
        <input type="hidden" name="CUSTOM_ORDER" value="Y">
        
        <input type="submit" value="�������� �����">
      </form>
    </div>
  <?endif;?>
<?endif;?>
<?if($_POST['AJAX_CALL']) {
  die();
}?>


