<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $USER;

use Bitrix\Main,    
    Bitrix\Main\Web\Json,    
    Bitrix\Main\Context;
    
//////////////////////
$request = Context::getCurrent()->getRequest();
$phone = $request->getPost("phone");   
$offerID = $request->getPost("offerID"); 
$quantity = $request->getPost("quantity"); 
if(!$quantity) {
  $quantity = 1;
}
$response = array();

if(!preg_match("/^(\+)?([0-9]{2,6}\s?){2,5}$/", $phone)) {
    $response["errors"][] = "��������� ������������ ��������";
}
if(empty($offerID)) {  
    $response["errors"][] = "�� ������ ID ������";
}
if(empty($quantity)) {
    $response["errors"][] = "�� ������ ���������� ������";
}
if ($response["errors"]) {
    $jsonString = Json::encode($response);
    echo $jsonString;
    die();
}

Bitrix\Main\Loader::includeModule('sale');
Bitrix\Main\Loader::includeModule('catalog');
        
$basket = Bitrix\Sale\Basket::create(SITE_ID);

if ($item = $basket->getExistsItem('catalog', $offerID)){
   $item->setField('QUANTITY', $item->getQuantity() + $quantity);
}else{
   $item = $basket->createItem('catalog', $offerID);
   $item->setFields([
      'QUANTITY' => $quantity,
      'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
      'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
      'PRODUCT_PROVIDER_CLASS' => \Bitrix\Catalog\Product\Basket::getDefaultProviderName() ,
   ]);
}

global $USER;
if ($USER->IsAuthorized()) 
    $userID = $USER->GetID();
else
    $userID = CSaleUser::GetAnonymousUserID();

$order = Bitrix\Sale\Order::create(SITE_ID, $userID);
$order->setPersonTypeId(1);
$order->setBasket($basket);


$shipmentCollection = $order->getShipmentCollection();
$shipment = $shipmentCollection->createItem(
    Bitrix\Sale\Delivery\Services\Manager::getObjectById(1)
);

$shipmentItemCollection = $shipment->getShipmentItemCollection();

/** @var Sale\BasketItem $basketItem */

foreach ($basket as $basketItem)
{
    $item = $shipmentItemCollection->createItem($basketItem);
    $item->setQuantity($basketItem->getQuantity());
}

$paymentCollection = $order->getPaymentCollection();
$payment = $paymentCollection->createItem(
    Bitrix\Sale\PaySystem\Manager::getObjectById(1)
);
$payment->setField("SUM", $order->getPrice());
$payment->setField("CURRENCY", $order->getCurrency());

$propertyCollection = $order->getPropertyCollection();
$phonePropValue = $propertyCollection->getPhone();
$phonePropValue->setValue($phone);

$result = $order->save();
if (!$result->isSuccess())
{
    $result->getErrors();
}
$jsonString = Json::encode(true);
echo $jsonString;
die();
/////////////////

