
// var a = BX.message('TEMPLATE_PATH');

BX.ready(function(){
	var popupFastOrder = new BX.PopupWindow('fast-order', window.body, {
		autoHide : true,
		lightShadow : true,
		overlay: true,
		closeIcon : true,
		closeByEsc : true,
		buttons: [
		new BX.PopupWindowButton({
			text: "�������� �����",
			className: "button-fast-order",
			events: {click: function(){
				var elemID = BX('current-offer-id').innerHTML;
		        var quantity = document.getElementById("elem-quantity").getElementsByTagName("input")[0].value;;
		        BX.adjust(BX('fast-order-offer-id'), {attrs: {value: elemID}});
		        BX.adjust(BX('fast-order-offer-quantity'), {attrs: {value: quantity}});
				BX.ajax.submit(BX("fast-order-form"), function(data){
					var result = JSON.parse(data);
					var response = '';
					if(result.errors) {
						result.errors.forEach(
							error => 
							response += "<div style='color:red'>"+error+"</div>"
							);

						BX("response-fast-order").innerHTML = response;
					} else {
						var mess = '<div class="fast-order-title">����� ��������</div>';
						BX("fast-order").innerHTML = mess;
					}					
				});
			}}
		}),
	 ]
	});
   popupFastOrder.setContent(BX('fast-order-block'));
   BX.bindDelegate(
      document.body, 'click', {className: 'button-init-fast-order' },
         BX.proxy(function(e){
            if(!e)
               e = window.event;

            popupFastOrder.show();
            return BX.PreventDefault(e);
         }, popupFastOrder)
   );
});