<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arFilter = array(
	"ACTIVE_DATE" => "Y", 
	"ACTIVE" => "Y", 
	"!ID" => $arParams["ELEMENT_ID"], 
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"IBLOCK_SECTION_ID" => $arParams["SECTION_ID"],
); 
$arSelect = array(
  "ID",
  "NAME",
  "DETAIL_PAGE_URL",
  "DETAIL_PICTURE",
  "PRICE_1",
);

$elementItems = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 5), $arSelect);
while($item = $elementItems->GetNext()) {
	$prevPict  = CFile::ResizeImageGet($item["DETAIL_PICTURE"], array('width'=>150, 'height'=>200), BX_RESIZE_IMAGE_PROPORTIONAL, true);                      
	
	$arButtons = CIBlock::GetPanelButtons($arParams['IBLOCK_ID'], $item['ID'], false, array("SECTION_BUTTONS"=>false, "SESSID"=>false));
	$item["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
	$item["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
	$item["PRINT_PRICE"] = rtrim(rtrim($item["PRICE_1"], '0'), '.')." ���.";
	$item["PREVIEW_PICTURE"] = $prevPict;
	$item["DETAIL_PAGE_URL"] = CComponentEngine::MakePathFromTemplate(
	    $arParams["TEMPLATE_PATH"], 
	    array(
	        "SECTION_ID" => $item["IBLOCK_SECTION_ID"],
	        "ELEMENT_ID" => $item["ID"],
	        "ELEMENT_CODE" => $item["CODE"],
	    )
	);
	$arResult["ITEMS"][] = $item;
}
$this->IncludeComponentTemplate();