<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 */

$this->setFrameMode(true);
$this->addExternalCss('/bitrix/css/main/bootstrap.css');

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_CPV_TPL_ELEMENT_DELETE_CONFIRM'));

$labelPositionClass = '';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

foreach ($arResult['ITEMS'] as $item)
{
	$uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
	$areaIds[$item['ID']] = $this->GetEditAreaId($uniqueId);
	$this->AddEditAction($uniqueId, "/bitrix/admin/currency_edit.php?lang=ru&ID=".$item['ID']."&bxpublic=Y", $elementEdit);
	$this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
	?>
		<a id="<?=$this->GetEditAreaId($uniqueId);?>" href="<?=$item["DETAIL_PAGE_URL"]?>">
			<span class="relative-img" style="background-image: url(<?=$item["PREVIEW_PICTURE"]["SRC"]?>);"></span>
			<span class="relative-title"><?=$item["NAME"]?></span>
			<span class="relative-title"><?=$item["OFFERS"][0]["ITEM_PRICES"][0]["PRINT_PRICE"]?></span>
		</a>
	<?
	
}
?>
		