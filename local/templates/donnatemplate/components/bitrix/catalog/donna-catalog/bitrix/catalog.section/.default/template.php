<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["DESCRIPTION"])):?>
	<div class="catalog-top">
		<div class="catalog-top-text">
			<h1><?=$arResult["NAME"];?></h1>
			<p><?=$arResult["DESCRIPTION"];?></p>
		</div>
		<div class="offer">
			<a href="<?=SITE_DIR.'about/opt'?>"><img src="<?=SITE_DIR?>include/opt.jpg" alt=""></a>
		</div>
	</div>
<?endif;?>

<div class="catalog-list">
	<div class="sort">
		<div class="sort-left">
			    
			�����������: 
			<?
			$sortPriceMethod = "asc";
			if ($_GET["sort"] == "catalog_PRICE_1") {
				$sortPriceClass = " active";
				if ($_GET["method"] == "asc") {
			 	$sortPriceClass .= " s-top";
			 	$sortPriceMethod = "desc";
			} else {
				$sortPriceClass .= " s-bottom";
			}
			}
			$sortNewMethod = "asc";
			if ($_GET["sort"] == "propertysort_NEWPRODUCT") {
				$sortNewClass = " active";
				if ($_GET["method"] == "asc") {
			 	$sortNewClass .= " s-top";
			 	$sortNewMethod = "desc";
			} else {
				$sortNewClass .= " s-bottom";
			}
			}
			?>			 
			 <a class="<?=$sortPriceClass?>" href="<?=$arResult["SECTION_PAGE_URL"]?>?sort=catalog_PRICE_1&method=<?=$sortPriceMethod?>">�� ����</a> 
			 <a class="<?=$sortNewClass?>" href="<?=$arResult["SECTION_PAGE_URL"]?>?sort=propertysort_NEWPRODUCT&method=<?=$sortNewMethod?>" >�� �������</a>
		</div>
		<div class="sort-right">
			<?if($arParams["DISPLAY_TOP_PAGER"]):?>
				<?=$arResult["NAV_STRING"]?><br />
			<?endif;?>
		</div>
	</div>
	<div class="catalog-section">
		<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="goods" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
			<div class="goods-inner">
				<?
				$elementDescription = array();
				$elementDescription["ARTNUMBER"] = $arElement["PROPERTIES"]["ARTNUMBER"]["VALUE"];
				if($arParams["PRODUCT_DISPLAY_MODE"] == "N") {
					$morePhoto = $arElement["MORE_PHOTO"];
				}
				foreach ($arElement["OFFERS"] as $value) {
					if(empty($elementDescription["PRICE"])) {
						$elementDescription["PRICE"] = $value["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE"];
					}
					$valueSize = '';
					if(!empty($value["PROPERTIES"]["SIZES_SHOES"]["VALUE"])) {
						$valueSize = $value["PROPERTIES"]["SIZES_SHOES"]["VALUE"];
					} elseif(!empty($value["PROPERTIES"]["SIZES_CLOTHES"]["VALUE"])) {
						$valueSize = $value["PROPERTIES"]["SIZES_CLOTHES"]["VALUE"];
					}

					if($arParams["PRODUCT_DISPLAY_MODE"] != "N") {
						$morePhoto = $value["MORE_PHOTO"];
					}
					$elementDescription["OFFERS"][] = array(
						"ARTNUMBER" => $value["PROPERTIES"]["ARTNUMBER"]["VALUE"],
						"PRINT_PRICE" => $value["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE"],
						"SIZE" => $valueSize,
						"MORE_PHOTO" => $morePhoto,
					);
				}?>
				<div class="goods-slider">

            <?
            if ($arElement['LABEL'])
            {
                ?>
              <div class="product-item-label-text <?=$arParams["LABEL_PROP_POSITION"]?>">
                  <?

                  if (!empty($arElement['LABEL_ARRAY_VALUE']))
                  {
                      foreach ($arElement['LABEL_ARRAY_VALUE'] as $code => $value)
                      {
                          switch ($code) {
                              case "SALEPRODUCT":
                                  ?>
                                <div class="sale" title="<?=$value?>"></div>
                                  <?
                                  break;
                              case "NEWPRODUCT":
                                  ?>
                                <div class="new" title="<?=$value?>"></div>
                                  <?
                                  break;
                              case "HITPRODUCT":
                                  ?>
                                <div class="hit" title="<?=$value?>"></div>
                                  <?
                                  break;
                          }
                      }
                  }
                  ?>
              </div>
                <?
            }
            ?>

					<ul class="slides">
						<?foreach ($elementDescription["OFFERS"][0]["MORE_PHOTO"] as $value):?>
								<li style="background-image: url(<?=$value["SRC"]?>);"></li>
						<?endforeach;?>
						
					</ul>
					<a href="<?=$arElement["DETAIL_PAGE_URL"]?>?ajax-elem=Y" class="quick-view various fancybox.ajax" data-fancybox-type="ajax"><?echo GetMessage("CATALOG_QUICK_VIEW")?></a>
				</div>
				<div class="goods-description">
					<h3><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></h3>
					
					<div class="art">�������: 
						<span><?=$elementDescription["ARTNUMBER"]?></span>
					</div>
					<div class="cost"><?=$elementDescription["PRICE"]?></div>
					<div class="sizes">
						<div>
							 �������:
						</div>
						<ul>
							<?foreach ($elementDescription["OFFERS"] as $value):?>
								<li artnumber='<?=$value["ARTNUMBER"]?>' cost='<?=$value["PRINT_PRICE"]?>'>
									<?=$value["SIZE"]?>
								</li>
							<?endforeach;?>
						</ul>
						<?if($arParams["DISPLAY_COMPARE"]):?>
							<div class="add-compare">
								<a href="<?echo $arElement["COMPARE_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_COMPARE")?></a>
							</div>
						<?endif?>
					</div>
				</div>
			</div>
		</div>
		<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>



	</div>
	<div class="sort last">
		<div class="sort-left">
			<a href="#" class="down">������</a>
		</div>
		<div class="sort-right">
			<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
				<br /><?=$arResult["NAV_STRING"]?>
			<?endif;?>
	</div>
</div>