  
  <?if($APPLICATION->GetCurPage() != "/"){?>

    </div>
      
  <?}?>
</section>
<!-- start footer -->
	<footer id="footer">	
    <div class="inner">
      <div class="footer-phones">
        <div class="footer-phone">
          <?
          $APPLICATION->IncludeFile(
            SITE_DIR."include/footer-phone-retail.php",
            Array(),
            Array("MODE"=>"html")
          );
          ?>
        </div>
        
        <div class="footer-phone">
         <?
          $APPLICATION->IncludeFile(
            SITE_DIR."include/footer-phone-wholesale.php",
            Array(),
            Array("MODE"=>"html")
          );
          ?>
        </div>
      </div>
      
      <div class="info-menu">
          <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            ".default",
            array(
              "ROOT_MENU_TYPE" => "info",
              "MAX_LEVEL" => "1",
              "CHILD_MENU_TYPE" => "top",
              "USE_EXT" => "N",
              "DELAY" => "N",
              "ALLOW_MULTI_SELECT" => "Y",
              "MENU_CACHE_TYPE" => "N",
              "MENU_CACHE_TIME" => "3600",
              "MENU_CACHE_USE_GROUPS" => "Y",
              "MENU_CACHE_GET_VARS" => array(
              ),
              "COMPONENT_TEMPLATE" => ".default"
            ),
            false
          );?>
      </div>
      
      <div class="company-menu">

          <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "company-menu",
            array(
              "ROOT_MENU_TYPE" => "company",
              "MAX_LEVEL" => "1",
              "CHILD_MENU_TYPE" => "top",
              "USE_EXT" => "N",
              "DELAY" => "N",
              "ALLOW_MULTI_SELECT" => "Y",
              "MENU_CACHE_TYPE" => "N",
              "MENU_CACHE_TIME" => "3600",
              "MENU_CACHE_USE_GROUPS" => "Y",
              "MENU_CACHE_GET_VARS" => array(
              ),
              "COMPONENT_TEMPLATE" => "company-menu"
            ),
            false
          );?>
      </div>
      
      <div class="social">
      
        <?
        $APPLICATION->IncludeFile(
          SITE_DIR."include/footer-social-link.php",
          Array(),
          Array("MODE"=>"html")
        );
        ?>
        
        <div class="subscribe-section">
          <!-- start form -->
          <?$APPLICATION->IncludeComponent(
	"bitrix:sender.subscribe", 
	".default", 
	array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CONFIRMATION" => "Y",
		"HIDE_MAILINGS" => "N",
		"SET_TITLE" => "N",
		"SHOW_HIDDEN" => "N",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"USE_PERSONALIZATION" => "Y",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
          <!-- end of form -->
        </div>
      </div>
      <div class="copyright"><?
      $APPLICATION->IncludeFile(
        SITE_DIR."include/copyright.php",
        Array(),
        Array("MODE"=>"html")
      );
      ?></div>
    </div>
	</footer>
	<!-- end of footer -->
	
</section>
<!-- end of wrapper -->

</body>
</html>
