<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?$APPLICATION->ShowTitle()?></title>
<?
  use Bitrix\Main\Page\Asset;
  $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.fancybox.css");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/jquery-1.11.3.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/jquery.flexslider.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/jquery-ui.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/slick.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/jquery.fancybox.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/jquery.zoom.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/jquery.fancybox-media.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/scripts/scripts.js");
  $APPLICATION->ShowHead();
  ?>
</head>
<body>
 <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<section id="wrapper">
  
  <!-- start header -->
  <header id="header">	
    <div class="inner">
      <div id="logo">
        <a href="<?=SITE_DIR?>" title="Site name"><!-- logo should be used as background --></a>
      </div>			
      
      <div class="search-section">
        <!-- start form -->
          <?$APPLICATION->IncludeComponent(
	"bitrix:search.form", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PAGE" => "#SITE_DIR#catalog/",
		"USE_SUGGEST" => "N"
	),
	false
);?>
        <!-- end of form -->
      </div>
      
      <div class="phones">
        <div class="phone">
          <?
          $APPLICATION->IncludeFile(
            SITE_DIR."include/header-phone-retail.php",
            Array(),
            Array("MODE"=>"html")
          );
          ?>
        </div>
        
        <div class="phone">
          <?
          $APPLICATION->IncludeFile(
            SITE_DIR."include/header-phone-wholesale.php",
            Array(),
            Array("MODE"=>"html")
          );
          ?>
        </div>
      </div>



      <div class="acount-info">
          <?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line", 
	"header", 
	array(
		"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
		"PATH_TO_PERSONAL" => SITE_DIR."personal/",
		"SHOW_PERSONAL_LINK" => "N",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_TOTAL_PRICE" => "Y",
		"SHOW_PRODUCTS" => "N",
		"POSITION_FIXED" => "N",
		"SHOW_AUTHOR" => "Y",
		"PATH_TO_REGISTER" => SITE_DIR."login/",
		"PATH_TO_PROFILE" => SITE_DIR."personal/",
		"COMPONENT_TEMPLATE" => "header",
		"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
		"SHOW_EMPTY_VALUES" => "Y",
		"PATH_TO_AUTHORIZE" => "",
		"SHOW_REGISTRATION" => "Y",
		"HIDE_ON_BASKET_PAGES" => "N",
		"SHOW_DELAY" => "N",
		"SHOW_NOTAVAIL" => "N",
		"SHOW_IMAGE" => "Y",
		"SHOW_PRICE" => "Y",
		"SHOW_SUMMARY" => "Y",
		"POSITION_HORIZONTAL" => "right",
		"POSITION_VERTICAL" => "top",
		"MAX_IMAGE_SIZE" => "90"
	),
	false
);?>
      </div>
      
      <div class="mobile-menu"><span></span><div>����</div></div>
    </div>			
  </header>
  <!-- end of header -->


  <!-- start navigation -->
  <nav id="navi">
    <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	".default", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "top",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
  </nav>
  <!-- end of navigation -->

<section id="container">
  <?if($APPLICATION->GetCurPage() != "/"){?>

    <div class="inner">
      <?$APPLICATION->IncludeComponent(
        "bitrix:breadcrumb",
        "catalog-donna",
        Array(
          "PATH" => "",
          "SITE_ID" => "s1",
          "START_FROM" => "0"
        )
      );?>

  <?}?>